/*
CREATE DATABASE timer;
USE timer;
*/

CREATE TABLE account (
    id VARCHAR(255) NOT NULL,
    last_used TIMESTAMP NOT NULL,

    PRIMARY KEY(id)
);

CREATE TABLE actor (
    id VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL,

    PRIMARY KEY(id)
);

CREATE TABLE account_actor (
    account_id VARCHAR(255) NOT NULL,
    actor_id VARCHAR(255) NOT NULL,

    PRIMARY KEY(account_id, actor_id),
    FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE,
    FOREIGN KEY (actor_id) REFERENCES actor(id) ON DELETE CASCADE
);

CREATE TABLE act (
    id VARCHAR(255) NOT NULL,
    start BIGINT NOT NULL,
    end BIGINT,

    PRIMARY KEY(id)
);

CREATE TABLE actor_act (
    actor_id VARCHAR(255) NOT NULL,
    act_id VARCHAR(255) NOT NULL,

    PRIMARY KEY(actor_id, act_id),
    FOREIGN KEY (actor_id) REFERENCES actor(id) ON DELETE CASCADE,
    FOREIGN KEY (act_id) REFERENCES act(id) ON DELETE CASCADE
);