package an.timer.backend.constants;

public class WebConstants {
    public static final String WEB_SOCKET_END_POINT = "/socket";
    public static final String WEB_SOCKET_PUBLIC_PREFIX = "/topic";
    public static final String WEB_SOCKET_PRIVATE_PREFIX = "/queue";
}
