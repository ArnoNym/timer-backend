package an.timer.backend.data.models;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class Act {
    @Id
    private String id;

    @NotNull
    private Long start;

    private Long end;
}
