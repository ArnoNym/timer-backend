package an.timer.backend.data.models;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class Actor {
    @Id
    private String id;

    @NotEmpty
    private String name;

    @Transient
    private List<Act> acts = new ArrayList<>();

    public Actor(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
