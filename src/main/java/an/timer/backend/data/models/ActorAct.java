package an.timer.backend.data.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor

@Entity
@IdClass(ActorAct.class)
public class ActorAct implements Serializable {
    @Id
    private String actorId;

    @Id
    private String actId;
}
