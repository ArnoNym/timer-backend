package an.timer.backend.data.models;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class Account {
    @Id
    private String id;
    @NotNull
    private Timestamp lastUsed;
}
