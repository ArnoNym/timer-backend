package an.timer.backend.data.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor

@Entity
@IdClass(AccountActor.class)
public class AccountActor implements Serializable {
    @Id
    private String accountId;

    @Id
    private String actorId;
}
