package an.timer.backend.data.daos;

import an.timer.backend.data.models.AccountActor;
import an.timer.backend.data.models.Act;
import an.timer.backend.data.models.Actor;
import an.timer.backend.data.models.ActorAct;
import an.timer.backend.data.repositories.AccountActorRepository;
import an.timer.backend.data.repositories.ActRepository;
import an.timer.backend.data.repositories.ActorActRepository;
import an.timer.backend.data.repositories.ActorRepository;
import an.timer.backend.utils.OptionalPair;
import an.timer.backend.utils.Utils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@AllArgsConstructor

@Component
public class ActorDao {
    private final AccountActorRepository accountActorRepository;
    private final ActorRepository actorRepository;
    private final ActorActRepository actorActRepository;
    private final ActRepository actRepository;

    public List<Actor> getActors(String accountId) {
        List<Actor> actors = actorRepository.findAllByAccountId(accountId);
        for (Actor actor : actors) {
            actor.setActs(actRepository.findAllByActorId(actor.getId()));
        }
        return actors;
    }

    public Actor newActor(String accountId, String actorName) {
        String actorId = Utils.uuid();
        Actor actor = actorRepository.save(new Actor(actorId, actorName));
        accountActorRepository.save(new AccountActor(accountId, actorId));
        return actor;
    }

    public void deleteActor(String actorId) {
        Set<String> actIds = actRepository.findAllByActorId(actorId)
                .stream()
                .map(Act::getId)
                .collect(Collectors.toSet());
        actRepository.deleteAllByIdIn(actIds); // Split up due to MySql shenanigans
        actorRepository.deleteById(actorId);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public OptionalPair<Act, Act> newStart(String accountId, String actorId) {
        System.out.println("START ActorDao.newStart");

        Act endedAct;
        Optional<Act> optionalCurrentAct = getCurrent(accountId);
        if (optionalCurrentAct.isPresent()) {
            Act currentAct = optionalCurrentAct.get();

            if (currentAct.getId().equals(actorId)) {
                return OptionalPair.empty();
            }

            currentAct.setEnd(Instant.now().toEpochMilli());
            endedAct = actRepository.save(currentAct);
        } else {
            endedAct = null;
        }

        String actId = Utils.uuid();
        Act startedAct = actRepository.save(new Act(actId, Instant.now().toEpochMilli(), null));
        actorActRepository.save(new ActorAct(actorId, actId));

        System.out.println("END ActorDao.newStart");
        return OptionalPair.ofNullable(startedAct, endedAct);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Act newEnd(@NotNull String actId) {
        System.out.println("START ActorDao.newEnd");

        Act act = actRepository.findById(actId).orElseThrow();
        act.setEnd(Instant.now().toEpochMilli());
        actRepository.save(act);

        System.out.println("END ActorDao.newEnd");
        return act;
    }

    public Optional<Act> getCurrent(String accountId) {
        Set<Act> currentActs = actRepository.findAllByAccountIdAndEndIsNull(accountId);
        if (currentActs.isEmpty()) {
            return Optional.empty();
        }
        if (currentActs.size() > 1) {
            throw new IllegalStateException();
        }
        return Optional.of(currentActs.iterator().next());
    }
}
