package an.timer.backend.data.daos;

import an.timer.backend.data.models.Account;
import an.timer.backend.data.repositories.AccountRepository;
import an.timer.backend.utils.Utils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.Instant;

@AllArgsConstructor

@Component
public class AccountDao {
    private final AccountRepository accountRepository;

    public Account get(String id) {
        Account account = accountRepository.findById(id).orElseThrow();
        account.setLastUsed(Timestamp.from(Instant.now()));
        accountRepository.save(account);
        return account;
    }

    public Account create() {
        String accountId = Utils.uuid();
        return accountRepository.save(new Account(accountId, Timestamp.from(Instant.now())));
    }


    /*
    private final UserRepo userRepo;
    private final PlayerRepo playerRepo;

    public User newUser(String email, String userName, String password) throws UsernameNotFoundException {
        User user = userRepo.save(new User(Utils.uuid(), email, userName, password, Timestamp.from(Instant.now()), null));
        Player player = playerRepo.save(new Player(user.getId(), PlayerAccessRights.PRIVATE, Utils.accessToken(), Utils.accessToken(), Utils.accessToken()));
        user.getPlayers().add(player);
        return user;
    }

    public User getUserByUserName(String userName) {
        return userRepo.findFirstByName(userName);
    }
    */

    /*
    public static UserDetails createUserDetails(User user) {
        return new org.springframework.security.core.userdetails.User(user.getName(), user.getPassword(), new ArrayList<>());
    }
    */
}
