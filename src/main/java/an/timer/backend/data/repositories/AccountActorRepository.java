package an.timer.backend.data.repositories;

import an.timer.backend.data.models.AccountActor;
import org.springframework.data.repository.CrudRepository;

public interface AccountActorRepository extends CrudRepository<AccountActor, String> {
}
