package an.timer.backend.data.repositories;

import an.timer.backend.data.models.ActorAct;
import org.springframework.data.repository.CrudRepository;

public interface ActorActRepository extends CrudRepository<ActorAct, String> {
}
