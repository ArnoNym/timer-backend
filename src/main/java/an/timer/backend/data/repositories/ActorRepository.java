package an.timer.backend.data.repositories;

import an.timer.backend.data.models.Actor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ActorRepository extends CrudRepository<Actor, String> {
    @Query(
            value = "SELECT new Actor(a.id, a.name) " +
                    "FROM AccountActor aa, Actor a " +
                    "WHERE aa.accountId = ?1 AND aa.actorId = a.id " +
                    "ORDER BY a.name"
    )
    List<Actor> findAllByAccountId(String accountId);
}
