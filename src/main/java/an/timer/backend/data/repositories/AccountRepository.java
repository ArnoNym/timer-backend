package an.timer.backend.data.repositories;

import an.timer.backend.data.models.Account;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, String> {
}
