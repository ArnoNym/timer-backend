package an.timer.backend.data.repositories;

import an.timer.backend.data.models.Act;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface ActRepository extends CrudRepository<Act, String> {

    @Transactional
    @Modifying
    void deleteAllByIdIn(Collection<String> ids);

    @Query(
            value = "SELECT new Act(a.id, a.start, a.end) " +
                    "FROM ActorAct aa, Act a " +
                    "WHERE aa.actorId = ?1 AND aa.actId = a.id " +
                    "ORDER BY a.start"
    )
    List<Act> findAllByActorId(String actorId);

    @Query(
            value = "SELECT new Act(a.id, a.start, a.end) " +
                    "FROM AccountActor acca, ActorAct aa, Act a " +
                    "WHERE acca.accountId = ?1 AND acca.actorId = aa.actorId AND aa.actId = a.id AND a.end IS NULL "
    )
    Set<Act> findAllByAccountIdAndEndIsNull(String actorId);
}
