package an.timer.backend.web;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {
    /**
     * Das aktiviert CORSE. Browser blockieren das holen von Daten von anderen Websites (oder Ports), also
     * blockieren sie CORSE, weshalb ich es zumindest fuers Entwickeln deaktivieren muss.
     * https://stackoverflow.com/questions/44697883/can-you-completely-disable-cors-support-in-spring
     *
     * ZUSATETZLICH Muss durch Spring Security in WebSecurityConfiguration cors erlaubt werden
     *
     * ZUSAETZLICH Muss Cors auch fuer WebSockets erlaubt werden
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedMethods("*");
    }
}
