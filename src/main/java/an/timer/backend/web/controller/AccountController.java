package an.timer.backend.web.controller;

import an.timer.backend.data.daos.AccountDao;
import an.timer.backend.data.models.Account;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor

@RestController
public class AccountController {
    private final AccountDao accountDao;

    @PostMapping("/createAccount")
    public Account createAccount() {
        System.out.println("CALLED ::: createAccount");
        return accountDao.create();
    }

    @PostMapping("/getAccount")
    public Account createAccount(@RequestBody String accountId) {
        System.out.println("CALLED ::: getAccount");
        return accountDao.get(accountId);
    }
}
