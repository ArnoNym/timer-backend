package an.timer.backend.web.controller;

import an.timer.backend.constants.WebConstants;
import an.timer.backend.data.daos.ActorDao;
import an.timer.backend.data.models.Act;
import an.timer.backend.data.models.Actor;
import an.timer.backend.utils.OptionalPair;
import an.timer.backend.web.wrapper.AccountIdActIdWrapper;
import an.timer.backend.web.wrapper.AccountIdActorIdWrapper;
import an.timer.backend.web.wrapper.AccountIdActorNameWrapper;
import an.timer.backend.web.wrapper.AddStartReturnWrapper;
import lombok.AllArgsConstructor;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
public class ActorController {
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final ActorDao actorDao;

    @PostMapping("/getActors")
    public List<Actor> getActors(@RequestBody String accountId) {
        return actorDao.getActors(accountId);
    }

    @MessageMapping("/newActor")
    public void newActor(@RequestBody @Valid AccountIdActorNameWrapper wrapper) {
        Actor actor = actorDao.newActor(wrapper.getAccountId(), wrapper.getActorName());
        simpMessagingTemplate.convertAndSend(WebConstants.WEB_SOCKET_PRIVATE_PREFIX+"/addActor"+"/"+wrapper.getAccountId(), actor);
    }

    @MessageMapping("/deleteActor")
    public void removeActor(@RequestBody AccountIdActorIdWrapper wrapper) {
        actorDao.deleteActor(wrapper.getActorId());
        simpMessagingTemplate.convertAndSend(WebConstants.WEB_SOCKET_PRIVATE_PREFIX+"/removeActor"+"/"+wrapper.getAccountId(), wrapper.getActorId());
    }

    @MessageMapping("/newStart")
    public void newStart(@RequestBody AccountIdActorIdWrapper wrapper) {
        OptionalPair<Act, Act> optionalPair = actorDao.newStart(wrapper.getAccountId(), wrapper.getActorId());
        AddStartReturnWrapper returnWrapper = new AddStartReturnWrapper(
                wrapper.getActorId(),
                optionalPair.getV1().orElseThrow(),
                optionalPair.getV2().orElse(null)
        );
        System.out.println("Before answering to newStart threadCount="+java.lang.Thread.activeCount());
        simpMessagingTemplate.convertAndSend(WebConstants.WEB_SOCKET_PRIVATE_PREFIX+"/addStart"+"/"+wrapper.getAccountId(), returnWrapper);
    }

    @MessageMapping("/newEnd")
    public void newEnd(@RequestBody @Valid AccountIdActIdWrapper wrapper) {
        Act act = actorDao.newEnd(wrapper.getActId());
        System.out.println("Before answering to newEnd threadCount="+java.lang.Thread.activeCount());
        simpMessagingTemplate.convertAndSend(WebConstants.WEB_SOCKET_PRIVATE_PREFIX+"/addEnd"+"/"+wrapper.getAccountId(), act);
    }
}
