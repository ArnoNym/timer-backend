package an.timer.backend.web;

import an.timer.backend.constants.WebConstants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfiguration implements WebSocketMessageBrokerConfigurer {

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint(WebConstants.WEB_SOCKET_END_POINT)
                .setAllowedOrigins("http://localhost:8081") /* Erlaubt CORS Fuer Websockets */
                .withSockJS(); /* Wenn WebSockets nicht funktionieren werden die message pfade vorne um /... erweitert */
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.setApplicationDestinationPrefixes("/app"); /* Filtert alle Anfragen die damit anfangen und sendet sie
        an @MessageMapping */

        registry.enableSimpleBroker("/topic", "/queue"); /* Definierte Ziele um Messages zu senden und zu
        empfangen. topic ist oeffentlich und queue privat */
    }
}