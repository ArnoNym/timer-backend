package an.timer.backend.web.wrapper;

import an.timer.backend.data.models.Act;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Validated

@Getter
@AllArgsConstructor
public class AddStartReturnWrapper {
    @NotEmpty
    private final String actorId;
    @NotNull
    private final Act act;
    private final Act endedAct;
}
