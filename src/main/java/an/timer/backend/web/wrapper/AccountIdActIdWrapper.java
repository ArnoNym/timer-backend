package an.timer.backend.web.wrapper;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;

@Validated

@Getter
@AllArgsConstructor
public class AccountIdActIdWrapper {
    @NotEmpty
    private final String accountId;
    @NotEmpty
    private final String actId;
}
