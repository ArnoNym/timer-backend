package an.timer.backend.utils;

import java.util.UUID;

public class Utils {
    private static int id = 0;
    public static String uuid() {
        String idString = ""+id; //todo loeschen
        id++;
        return UUID.randomUUID().toString().replace("-", "")+"_"+idString;
    }

    public static String accessToken() {
        return uuid();
    }

    public static String userNameToPlayerName(String userName) {
        return userName + " Player";
    }
}

