package an.timer.backend.utils;

import java.util.Optional;

public class OptionalPair<V1, V2> {
    private final V1 v1;
    private final V2 v2;

    private OptionalPair(V1 v1, V2 v2) {
        this.v1 = v1;
        this.v2 = v2;
    }

    public Optional<V1> getV1() {
        return Optional.ofNullable(v1);
    }

    public Optional<V2> getV2() {
        return Optional.ofNullable(v2);
    }

    public boolean isV1Present() {
        return getV1().isPresent();
    }

    public boolean isV2Present() {
        return getV2().isPresent();
    }

    public boolean isPresent() {
        return isV1Present() && isV2Present();
    }

    public static <V1, V2> OptionalPair<V1, V2> empty() {
        return new OptionalPair<>(null, null);
    }

    public static <V1, V2> OptionalPair<V1, V2> of(V1 v1, V2 v2) {
        if (v1 == null || v2 == null) {
            throw new IllegalArgumentException();
        }
        return new OptionalPair<>(v1, v2);
    }

    public static <V1, V2> OptionalPair<V1, V2> ofNullable(V1 v1, V2 v2) {
        return new OptionalPair<>(v1, v2);
    }
}
